import java.util.Comparator;

public class SortListePerVoti implements Comparator<Party> {

	@Override
	public int compare(Party p1, Party p2) {
		
		if(p1.getVotiParz()<p2.getVotiParz()) return 1;
		
		else if(p1.getVotiParz()==p2.getVotiParz()) return 0;
		
		else return -1;
		
	}
	


}
