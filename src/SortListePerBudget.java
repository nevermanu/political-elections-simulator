import java.util.Comparator;

public class SortListePerBudget implements Comparator<Party> {

	@Override
	public int compare(Party p1, Party p2) {
		
		if(p1.getBudgetParz()<p2.getBudgetParz()) return 1;
		
		else if(p1.getBudgetParz()==p2.getBudgetParz()) return 0;
		
		else return -1;
		
	}
	


}