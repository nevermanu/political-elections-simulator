import java.util.ArrayList;
import java.util.List;

public class Movimento extends Party implements Caratteristiche {
	
	private int membri;
	private List<String> listacitta = new ArrayList<>();
	
	public Movimento(String nome, int voti) {
		super(nome,voti);
	}
	
	public Movimento(String nome, int voti, int budget, boolean playable) {
		super(nome,voti,budget,playable);
	}
	
	public Movimento(String nome, int voti, int budget, boolean playable, String leader, String orientamento, int membri, List<String> listacitta) {
		super(nome,voti,budget,playable,leader,orientamento);
		this.membri = membri;
		this.listacitta = listacitta;
	}

	public List<String> getListacitta() {
		return listacitta;
	}

	public void setListacitta(List<String> listacitta) {
		this.listacitta = listacitta;
	}

	public int getMembri() {
		return membri;
	}

	public void setMembri(int membri) {
		this.membri = membri;
	}
	
	@Override
	public void stampaCaratteristiche() {

		System.out.println("*****************************");
		System.out.println("*******CARATTERISTICHE*******");
		System.out.println("*****************************");
		System.out.println(this.getNome());
		System.out.println("*****************************\n");
		System.out.println("Leader: " + this.getLeader());
		System.out.println("Orientamento Politico: " + this.getOrientamento());
		System.out.println("Numero Membri: " + this.getMembri());
		System.out.println("Citt� Coinvolte:  " + this.getOrientamento());
		for(String citta:listacitta) {
			System.out.print(citta + ", ");
		}
		System.out.println("\n*****************************\n");
		
		
		
	}
}
