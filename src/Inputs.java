import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Inputs {
	
	private static int maxGiorni = 30;

	private static int numSeggi = 10;

	private static boolean inCoal = false;

	private static int maxBudget = 100_000_000;

	private static int startBudget = 1_000_000;

	public static List<Party> initListe(Scanner s, boolean game) {

		List<Party> listaParty = new ArrayList<Party>();

		boolean playable = false;
		boolean scelta = true;
		int budget = 0;
		int votes = 0;
		byte b;

		while (scelta) {

			if (game && !Inputs.inCoal) {

				System.out.println("\n\n//////////////////////////////////////////");
				System.out.println("**********VUOI COMANDARE QUESTA LISTA? S/N ***********");
				System.out.println("//////////////////////////////////////////\n\n");
				char c = s.next().charAt(0);

				if (c == 's' || c == 'S') {

					System.out.println("\n*** OK, COMANDEREI QUESTA LISTA. ***");
					playable = true;
				} else
					System.out.println("\n*** LA LISTA SARA' GESTITA DAL COMPUTER ***");

			}

		
		while (true) {

			System.out.println("\n\n//////////////////////////////////////////");
			System.out.println("**********SCEGLI TIPOLOGIA LISTA ***********");
			System.out.println("(1) Partito Nazionale");
			System.out.println("(2) Movimento");
			System.out.println("(3) Lista civica");
			System.out.println("(4) Coalizione");
			System.out.println("//////////////////////////////////////////\n\n");
			b = s.nextByte();

			if (b == 4 && Inputs.inCoal) {
				System.out.println("\n*** SCELTA NON ACCETTATA, STAI GIA FORMANDO UNA COALIZIONE ***");
			} else if(b>4||b==0) { 
				System.out.println("***SCELTA NON VALIDA, SI VEDE CHE SEI UN NAZIONALISTA***");
				break;
			}
				else break;

		}

		s.nextLine();

		System.out.println("\n\n//////////////////////////////////////////");
		System.out.println("**********INSERISCI NOME LISTA ***********");
		System.out.println("//////////////////////////////////////////\n\n");
		String nome = s.nextLine();

		String orientamento = null;
		String leader = null;
		if (b != 4 && !game) {

			System.out.println("\n\n//////////////////////////////////////////");
			System.out.println("********** INSERISCI VOTI LISTA *********"); 
			System.out.println("//////////////////////////////////////////\n\n");
			votes = s.nextInt();

			s.nextLine();

			while (true) {
				System.out.println("\n\n/////////////////////////////////////////////////");
				System.out.println("**********INSERISCI BUDGET DI PARTENZA ***********");
				System.out.println("//////////////////////////////////////////////////\n\n");
				budget = s.nextInt();
				s.nextLine();

				orientamento = "NULL";
				leader = "NULL";

				if (budget <= Inputs.getMaxBudget()) {
					break;
				} else
					System.out.println("*** BUDGET TROPPO ALTO (MAX " + Inputs.getMaxBudget() + ")");

			}

		} else if (b != 4 && game) {

			System.out.println("\n\n//////////////////////////////////////////////////");
			System.out.println("********** INSERISCI LEADER DELLA LISTA ***********");
			System.out.println("//////////////////////////////////////////////////\n\n");
			leader = s.nextLine();

			System.out.println("\n\n///////////////////////////////////////////////////////");
			System.out.println("********** INSERISCI ORIENTAMENTO POLITICO ***********");
			System.out.println("//////////////////////////////////////////////////////\n\n");
			orientamento = s.nextLine();

			votes = 0;
			budget = Inputs.getStartBudget();
		}

		switch (b) {
		case 1:
			if (game)
				listaParty.add(new Party(nome, votes, budget, playable, leader, orientamento));
			else
				listaParty.add(new Party(nome, votes, budget, playable));
			System.out.println("*** PARTITO NAZIONALE AGGIUNTO ! ***");
			break;
		case 2:
			if (game) {
				System.out.println("*** INSERISCI MEMBRI TOTALI DEL MOVIMENTO: ***");
				int membri = s.nextInt();

				List<String> listacitta = new ArrayList<>();
				while (true) {
					
					s.nextLine();
					System.out.println("*** INSERISCI CITTA' ADERENTE AL MOVIMENTO: ***");
					listacitta.add(s.nextLine());

					System.out.println("*** INSERIRE UN'ALTRA CITTA'? S/N ***");
					char sc = s.next().charAt(0);
					if (sc == 's' || sc == 'S')
						continue;
					else
						break;

				}
				listaParty.add(new Movimento(nome, votes, budget, playable, leader, orientamento, membri, listacitta));
			} else
				listaParty.add(new Movimento(nome, votes, budget, playable));
			System.out.println("*** MOVIMENTO AGGIUNTO ! ***");
			break;

		case 3:
			if (game) {
				System.out.println("*** INSERISCI CITTA DI PROVENIENZA DELLA LISTA CIVICA: ***");
				String citta = s.nextLine();
				listaParty.add(new ListaCivica(nome, votes, budget, playable, leader, orientamento, citta));
			} else
				listaParty.add(new ListaCivica(nome, votes, budget, playable));

			System.out.println("*** LISTA CIVICA AGGIUNTA ! ***");
			break;

		case 4:
			System.out.println("\n*** INIZIO COMPOSIZIONE COALIZIONE, PREMI INVIO PER COMINCIARE. ***");
			Inputs.inCoal = true;
			listaParty.add(new Coalizione(nome, s, playable,game));
			Inputs.inCoal = false;
			System.out.println("\n*** FINE COMPOSIZIONE COALIZIONE ***");
			System.out.println("\n*** COALIZIONE AGGIUNTA ! ***");
			break;

		default:
			if (game)
				listaParty.add(new Party(nome, votes, budget, playable, leader, orientamento));
			else
				listaParty.add(new Party(nome, votes, budget, playable));
			break;
		}

		while (true) {

			System.out.println("\n\n*** AGGIUNGERE UN'ALTRA LISTA? S/N ***\n\n");

			char c = s.next().charAt(0);

			if (c == 's' || c == 'S') {
				scelta = true;
				break;
			} else if (c == 'n' || c == 'N') {
				scelta = false;
				break;
			}
		}
		
		}
		return listaParty;
	}

	public static List<Servizio> initDefaultServizi() {

		List<Servizio> servizi = new ArrayList<>();
		servizi.add(new Servizio("Volantinaggio e Pubblicità"));
		servizi.add(new Servizio("Mass Media"));
		servizi.add(new Servizio("Organizzazione Eventi"));

		List<Pack> gammapack = new ArrayList<>();
		gammapack.add(new Pack("x1.000 volantini B&N", 0, 50, 100));
		gammapack.add(new Pack("x5.000 volantini B&N", 0, 200, 400));
		gammapack.add(new Pack("x10.000 volantini B&N", 0, 600, 800));
		
		List<Pack> betapack = new ArrayList<>();
		betapack.add(new Pack("x1.000 volantini a colori", 0, 50, 100));
		betapack.add(new Pack("x5.000 volantini a colori", 0, 200, 400));
		betapack.add(new Pack("x10.000 volantini a colori", 0, 600, 800));
		
		List<Pack> alfapack = new ArrayList<>();
		alfapack.add(new Pack("x10 Manifesto", 0, 50, 100));
		alfapack.add(new Pack("x30 Manifesti", 0, 200, 400));
		alfapack.add(new Pack("x50 Manifesti", 0, 600, 800));
		
		servizi.get(0).addAgenzia("Gamma", 1, gammapack);
		servizi.get(0).addAgenzia("Beta", 3, betapack);
		servizi.get(0).addAgenzia("alfa", 5, alfapack);
		
		List<Pack> tvpack = new ArrayList<>();
		tvpack.add(new Pack("Intervista in seconda serata", 100, 300, 1000));
		tvpack.add(new Pack("Pubblicità in prima serata", 250, 600, 3000));
		tvpack.add(new Pack("Intervista in prima serata", 400, 1000, 5000));
		
		List<Pack> radiopack = new ArrayList<>();
		radiopack.add(new Pack("Spezzone pubblicitario 10 secondi", 100, 300, 1000));
		radiopack.add(new Pack("Spezzone pubblicitario 20 secondi", 250, 600, 3000));
		radiopack.add(new Pack("Intervista Speaker Radiofonico", 400, 1000, 5000));
		
		List<Pack> giornpack = new ArrayList<>();
		giornpack.add(new Pack("Articolo breve", 100, 300, 1000));
		giornpack.add(new Pack("Articolo ampio", 250, 600, 3000));
		giornpack.add(new Pack("Prima pagina", 400, 1000, 5000));
		
		servizi.get(1).addAgenzia("Giornale", 1, giornpack);
		servizi.get(1).addAgenzia("Radio", 3, radiopack);
		servizi.get(1).addAgenzia("TV", 5, tvpack);
		
		List<Pack> giovepack = new ArrayList<>();
		giovepack.add(new Pack("Stand grande", 300, 800, 4000));
		giovepack.add(new Pack("Party per 5000 persone", 500, 1200, 6000));
		giovepack.add(new Pack("Comizio in piazza Duomo", 800, 2000, 8000));
		
		List<Pack> saturnopack = new ArrayList<>();
		saturnopack.add(new Pack("Stand medio", 300, 800, 4000));
		saturnopack.add(new Pack("Party per 3000 persone", 500, 1200, 6000));
		saturnopack.add(new Pack("Comizio in centro", 800, 2000, 8000));
		
		List<Pack> mercuriopack = new ArrayList<>();
		mercuriopack.add(new Pack("Stand piccolo", 300, 800, 4000));
		mercuriopack.add(new Pack("Party per 1000 persone", 500, 1200, 6000));
		mercuriopack.add(new Pack("Piccolo comizio", 800, 2000, 8000));
		
		servizi.get(2).addAgenzia("Mercury", 1, mercuriopack);
		servizi.get(2).addAgenzia("Saturn", 3, saturnopack);
		servizi.get(2).addAgenzia("Jupiter", 5, giovepack);
		
		return servizi;
	}
	
	public static Agenzia chooseAgenzia(List<Servizio> servizi,Byte ch, Scanner s) {
		
		byte agch;
		while (true) {
			System.out.println("\n\n/////////////////////////////////");
			System.out.println("****** SCELTA DELL'AGENZIA *******");
			System.out.println("/////////////////////////////////\n\n");
			System.out.println("\n\n/////////////////////////////////");
			
			for (Agenzia agenzia : servizi.get(ch).getListaAgenzie()) {

				System.out.println(
						"(" + servizi.get(ch).getMapAgenzie().get(agenzia) + ") " + agenzia.getNomeAgenzia());

			}
			System.out.println("\n\n/////////////////////////////////");
			agch = s.nextByte();
			
			if(agch > (servizi.get(ch).getListaAgenzie().size()-1)){
				System.out.println("*** SCELTA NON VALIDA ***");
				continue;
			}else break;
		}
		Agenzia agenzia = servizi.get(ch).getListaAgenzie().get(agch);
		
		return agenzia;
		
	}
	
	public static Pack choosePack(Agenzia agenzia, Scanner s){
		
		byte agpk;
		
		while(true) {
			
			System.out.println("\n\n///////////////////////////////////");
			System.out.println("****** SCELTA DEL PACCHETTO *******");
			System.out.println("///////////////////////////////////\n\n");
			System.out.println("\n\n/////////////////////////////////");
			
			for (Pack pack : agenzia.getListaPack()) {
				
				 int prezzo = (int) (pack.getPrezzo() * Day.getDailyMolt() * agenzia.getMoltAgenzia());
				 int minBUp= (int) (pack.getMinBU() * Day.getDailyMolt() * agenzia.getMoltAgenzia());
				 int maxBUp= (int) (pack.getMaxBU() * Day.getDailyMolt() * agenzia.getMoltAgenzia());
				 
				System.out.println("(" + (agenzia.getMapPack().get(pack)-1) + ") " + pack.getNome() + "--> "
						+ prezzo + " ("
						+ minBUp + " : "
						+ maxBUp + ")");
	
			}
			
			
			System.out.println("\n\n/////////////////////////////////");
			agpk = s.nextByte();
			
			if(agpk > (agenzia.getListaPack().size()-1)){
				System.out.println("*** SCELTA NON VALIDA ***");
				continue;
			}else break;
		}
		
		Pack pack = agenzia.getListaPack().get(agpk);
		return pack;
	}
	
	public static byte chooseService(List<Servizio> servizi,Scanner s) {
		
		byte ch;
		
		while(true) {
			System.out.println("\n\n/////////////////////////////////");
			System.out.println("****** SCELTA DEL SERVIZIO *******");
			System.out.println("/////////////////////////////////\n\n");
			
			System.out.println("\n\n/////////////////////////////////");
			System.out.println("(0) VOLANTINAGGIO E PUBBLICITA' ");
			System.out.println("(1) MASS MEDIA ");
			System.out.println("(2) ORGANIZZAZIONE EVENTI ");
			System.out.println("(3) ANNULLA ");
			System.out.println("\n\n/////////////////////////////////");
			ch = s.nextByte();
			
			if(ch > (servizi.size())){
				System.out.println("*** SCELTA NON VALIDA ***");
				continue;
			}else break;
		}
		
		return ch;
	}
	
	public static List<Pack> createSetPack(Scanner s){
		
		List<Pack> set = new ArrayList<>();
		
		while(true) {
			s.nextLine();
			System.out.println("*** INSERISCI NOME PACCHETTO: ***");
			String nome = s.nextLine();
			System.out.println("*** INSERISCI BACINO DI UTENZA MINIMO: ***");
			int minBU = s.nextInt();
			System.out.println("*** INSERISCI BACINO DI UTENZA MASSIMO: ***");
			int maxBU = s.nextInt();
			System.out.println("*** INSERISCI PREZZO: ***");
			int prezzo = s.nextInt();
			
			set.add(new Pack(nome,minBU,maxBU,prezzo));
			
			System.out.println("*** INSERIRE UN ALTRO PACCHETTO? S/N ***");
			char ch = s.next().charAt(0);
			
			if(ch=='s'||ch=='S') {
				continue;
			}else break;


		}
		
		return set;
	}
	
	public static int getStartBudget() {
		return maxBudget;
	}

	public static void setStartBudget(int val) {
		startBudget = val;

	}

	public static int getMaxBudget() {
		return maxBudget;
	}

	public static void setMaxBudget(int val) {
		maxBudget = val;
	}

	public static int getNumSeggi() {
		return numSeggi;
	}

	public static void setNumSeggi(int numSeggi) {

		Inputs.numSeggi = numSeggi;
	}

	public static int getMaxGiorni() {
		return maxGiorni;
	}

	public static void setMaxGiorni(int maxGiorni) {
		Inputs.maxGiorni = maxGiorni;
	}

}