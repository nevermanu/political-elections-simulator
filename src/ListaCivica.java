
public class ListaCivica extends Party implements Caratteristiche{
	
	private String citta = "N.P.";
	
	public ListaCivica(String nome, int voti, int budget, boolean playable,String leader, String orientamento) {
		super(nome,voti,budget,playable,leader,orientamento);
	}


	public ListaCivica(String nome, int voti, int budget, boolean playable,String leader, String orientamento,String citta) {
		super(nome,voti,budget,playable,leader,orientamento);
		this.citta=citta;
	}
	
	public ListaCivica(String nome, int voti, int budget, boolean playable) {
		super(nome,voti,budget,playable);
	}


	public String getCitta() {
		return this.citta;
	}
	
	public void setCitta(String citta) {
		this.citta=citta;
	}
	
	@Override
	public void stampaCaratteristiche() {

		System.out.println("*****************************");
		System.out.println("*******CARATTERISTICHE*******");
		System.out.println("*****************************");
		System.out.println(this.getNome());
		System.out.println("*****************************");
		System.out.println("Leader: " + this.getLeader());
		System.out.println("Orientamento Politico: " + this.getOrientamento());
		System.out.println("Citt� di provenienza: " + this.getCitta());
		System.out.println("*****************************\n");
		
	}

}
