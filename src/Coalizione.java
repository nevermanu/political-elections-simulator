
import java.util.List;
import java.util.Scanner;

public class Coalizione extends Party{
	
	private List<Party> membri;
	
	public Coalizione(String nome, Scanner s, boolean playable, boolean game) {
		
		super(nome);
		this.setPlayable(playable);
		this.membri = Inputs.initListe(s,game);
		
		int btot=0;
		int vtot=0;
		for(Party party:this.getMembri()) {
			btot+=party.getBudget();
			vtot+=party.getVotiTot();
		}
	
		this.setBudget(btot);
		this.setVotiTot(vtot);
			
	}
	
	public List<Party> getMembri(){
		return membri;
	}
	
	public List<Party> getC(){
		
		return membri;
	}
	
	@Override
	public void stampaCaratteristiche() {
		System.out.println("\n\n////////////////////////////////////////");
		System.out.println("*******CARATTERISTICHE COALIZIONE*******");
		System.out.println("////////////////////////////////////////");
		System.out.println("\t\t" + this.getNome());
		System.out.println("////////////////////////////////////////");
		int i=1;
		for(Party party: membri) {
				System.out.println("/////////////// MEMBRO " + i + " ///////////////");
				party.stampaCaratteristiche();
				i++;
				System.out.println("////////////////////////////////////////");
				System.out.println("\n");
				
		}
		System.out.println("////////////////////////////////////////\n\n");
	}

}

