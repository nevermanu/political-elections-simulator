
public class Pack {
	
	private String nome;
	private int minBU; //*** minimo bacino di utenza ***
	private int maxBU; //*** massimo bacino di utenza ***
	private int prezzo; //*** prezzo base pacchetto ***

	public Pack(String nome, int minBU, int maxBU, int prezzo) {
		this.nome=nome;
		this.minBU = minBU;
		this.maxBU = maxBU;
		this.prezzo = prezzo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getMinBU() {
		return minBU;
	}

	public void setMinBU(int minBU) {
		this.minBU = minBU;
	}

	public int getMaxBU() {
		return maxBU;
	}

	public void setMaxBU(int maxBU) {
		this.maxBU = maxBU;
	}

	public int getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(int prezzo) {
		this.prezzo = prezzo;
	}

}
