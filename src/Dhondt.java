import java.lang.Integer;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Dhondt {
	
	public static void execute(List<Party> listaParty, int totseggi) throws InterruptedException {
		
		
		Map<Party,Integer> map = new HashMap<>();      // *** HASH TABLE IN CUI MAPPARE PARTITI (CHIAVE) E RISPETTIVI DIVISORI (VALORE) *** //
		
			
		/////////////////////////////////////////////////
		// FASE DI INIZIALIZZAZIONE DEI VOTI PARZIALI //
		///////////////////////////////////////////////
		
		for(Party party: listaParty) {
			party.initVotiParz();  // *** COPIA DEI VOTI TOTALI IN QUELLI PARZIALI *** //
			map.put(party,1);     // *** DIVISORE INIZIALE FISSATO AD 1 *** //
			
		}
		
		
		//////////////////////////		
		// FASE DELL'ALGORITMO //
		////////////////////////

		
		for(int i = 0; i<(totseggi); i++) {
			
			Collections.sort(listaParty,new SortListePerVoti());      // *** SORT, ORDINA I PARTITI IN ORDINE DECR. IN BASE AI VOTI *** //
			
			System.out.println("\n\n\n//////////////// SEGGIO NUMERO " + (i+1) + " ////////////////\n\n\n");
			
			
			for(Party p: listaParty) {
				System.out.println("voti lista " + p.getNome() + ": "  + p.getVotiParz());
			}
			
			Party party = listaParty.get(0);	// *** SELEZIONA IL PARTITO CON PIU' VOTI, QUINDI QUELLO IN PRIMA POS. *** //
			
			System.out.println("scelta la lista " + party.getNome() + ", con " + party.getVotiParz() + " voti \n");
			
			party.setSeggi(party.getSeggi()+1);      // *** ASSEGNA UN SEGGIO AL PARTITO *** //
			
			System.out.println("seggio aggiunto! seggi attuali: " + party.getSeggi() + "\n");
			
			map.put(party,map.get(party)+1);       // *** AUMENTA IL DIVISORE MAPPATO NELLA HASH TABLE *** //
			
			System.out.println("divisore aggiornato ---> x" + map.get(party));
			
			party.setVotiParz(party.getVotiTot()/map.get(party));  // *** DIVIDE I VOTI TOTALI DI LISTA PER IL DIVISORE *** //
			
			System.out.println("voti parziali attuali: "+party.getVotiParz());
			
			Thread.sleep(500);
			
			
		}
		
		
		
		////////////////////////////////////////////////
		//SORT IN ORDINE DECRESCENTE IN BASE AI SEGGI //
		////////////////////////////////////////////////
		
		Collections.sort(listaParty,new SortListePerSeggi());
		
		
		/////////////////////////////////////
		// FASE DI STAMPA SEGGI ACQUISITI //
		///////////////////////////////////

		
		System.out.println("\n\n\n//////////////// RISULTATI ////////////////\n\n");
		
		for(Party party: listaParty) {
			
			System.out.println(party.getNome() + " ---> seggi acquisiti " + party.getSeggi());
			
			if(party instanceof Coalizione) {
			
				
				Dhondt.execute4money(party.getC(), party.getNome(), party.getSeggi());
			}
			
		}
	}
	

	

	private static void execute4money(List<Party> list ,String coalizione, int totseggi) {
		
		Map<Party,Integer> map = new HashMap<>();      // *** HASH TABLE IN CUI MAPPARE PARTITI (CHIAVE) E RISPETTIVI DIVISORI (VALORE) *** //
		
		
		/////////////////////////////////////////////////
		// FASE DI INIZIALIZZAZIONE DEL BUDGET PARZIALI //
		///////////////////////////////////////////////
		
		for(Party party: list) {
			
			party.setBudgetParz(party.getBudget());// *** COPIA DEL BUDGET TOT. IN QUELLO PARZIALE *** //
			map.put(party,1);     // *** DIVISORE INIZIALE FISSATO AD 1 *** //
			
		}
		
		
		//////////////////////////	
		// FASE DELL'ALGORITMO //
		////////////////////////

		
		for(int i = 0; i<(totseggi); i++) {
			
			Collections.sort(list,new SortListePerBudget());      // *** SORT, ORDINA I PARTITI IN ORDINE DECR. IN BASE AL BUDGET *** //
			

			
			Party party = list.get(0);	// *** SELEZIONA IL PARTITO CON IL BUDGET PIU ALTO, QUINDI QUELLO IN PRIMA POS. *** //
			
			
			party.setSeggi(party.getSeggi()+1);      // *** ASSEGNA UN SEGGIO AL PARTITO *** //

			
			map.put(party,map.get(party)+1);       // *** AUMENTA IL DIVISORE MAPPATO NELLA HASH TABLE *** //
			
			party.setBudgetParz(party.getBudget()/map.get(party));  // *** DIVIDE IL BUDGET TOTALE DI LISTA PER IL DIVISORE *** //
			
			
		}
		
		
		
		////////////////////////////////////////////////
		//SORT IN ORDINE DECRESCENTE IN BASE AI SEGGI //
		////////////////////////////////////////////////
		
		Collections.sort(list,new SortListePerSeggi());
		
		/////////////////////////////////////
		// FASE DI STAMPA SEGGI ACQUISITI //
		///////////////////////////////////

		System.out.println("\n**************************************************" );
		System.out.println("\n***COALIZIONE: " + coalizione.toUpperCase() + " ---> seggi individuali:" );
		
		for(Party party: list) {
			
			System.out.println(party.getNome() + " ---> seggi acquisiti " + party.getSeggi());
			
		}
		
		System.out.println("\n**************************************************" );
		
		
		
	}




}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
