
public class Day {
	
	private static float dailyMolt=(float) 0.1;
	
	private static int day=1;
	
	public static void incrementa() {
		 day+=1;
	}
	
	public static void reset() {
		day=1;
	}

	public static int getDay() {
		return day;
	}

	public static void setDay(int num) {
		 day = num;
	}
	
	public static float getDailyMolt() {
		return (float)(1+(day*dailyMolt));
	}
	
	public static void setDailyMolt(float m) {
		dailyMolt=m;
	}
	

}
