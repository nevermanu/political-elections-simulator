import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;

public class Elezioni {
	

	
	
	

	public static void main(String[] args) throws InterruptedException {
		
		Scanner s = new Scanner(System.in);
		
		List<Party> listaParty = new ArrayList<>();	
		List<Servizio> servizi = Inputs.initDefaultServizi(); 
		///////////////////////////////
		//    SCHERMATA INIZIALE    //
		/////////////////////////////
		System.out.println("////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("**********************************************************************************");
		System.out.println(" 		BENVENUTO SU SIM-ELECTION!  V 1.0          ");
		System.out.println(" 		                          author: Emanuele Albarino (2020)");
		System.out.println("**********************************************************************************");
		System.out.println("////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("////////////////////////////////////////////////////////////////////////////////////\n\n");
		
		///////////////
		//   MENU   //
		/////////////
		while(true) {
			
			byte scelta;

			
			
			System.out.println("\n\n//////////////////////////////////////////");
			System.out.println("******        MENU PRINCIPALE         *******");
			System.out.println("//////////////////////////////////////////");
			
			System.out.println("\n\n//////////////////////////////////////////");
			System.out.println("********** SELEZIONA MODALITA' ***********");
			System.out.println("(1) Avvia simulazione");
			System.out.println("(2) Metodo Dhondt");
			System.out.println("(3) Impostazioni");
			System.out.println("(0) Esci");
			System.out.println("//////////////////////////////////////////\n\n");
			scelta=s.nextByte();
			
			switch(scelta) {
			
				case 1:
					
					listaParty=Inputs.initListe(s,true);
					
					Play.execute(listaParty,servizi,s);
					
					for(Party party:listaParty) {
						party.stampaCaratteristiche();
					}
					
					Thread.sleep(1000);
					break;
					
					
				case 2:
					System.out.println("\n\n//////////////////////////////////////////");
					System.out.println("****** INSERISCI SEGGI DISPONIBILI *******");
					System.out.println("//////////////////////////////////////////\n\n");
					Inputs.setNumSeggi(s.nextInt());
					
					listaParty=Inputs.initListe(s,false);
					
					Dhondt.execute(listaParty, Inputs.getNumSeggi());
					
					for(Party party:listaParty) {
						party.stampaCaratteristiche();
					}
					
					break;
				
				
				case 3:
					
					Options.launch(servizi, s);
					break;

				case 0:
					System.out.println("*** ARRIVEDERCI ! ***");
					System.exit(0);
				
				default:
					System.out.println("*** SCELTA NON VALIDA ***\n\n");
					continue;
			}
			
			while(true) {
			
				System.out.println("\n\n//////////////////////////////////////////////");
				System.out.println("****** TORNARE AL MENU PRINCIPALE? S/N *******");
				System.out.println("//////////////////////////////////////////////\n\n");
				char ch = s.next().charAt(0);
				
				if(ch == 's'||ch == 'S') {
					
					listaParty.clear();
					break;
					
				}
				else if (ch == 'n'||ch == 'N') {
					System.out.println("*** ARRIVEDERCI ! ***");
					s.close();
					System.exit(0);
				}
				
				else System.out.println("*** SCELTA NON VALIDA ***");
				
			}
			
			
		}
		

	}

}
