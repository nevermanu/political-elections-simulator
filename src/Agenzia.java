import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Agenzia extends Servizio {
	
	
	private String nomeAgenzia;
	private int moltAgenzia;
	private List<Pack> listaPack = new ArrayList<>();
	private Integer cntmap=0;
	private Map<Pack,Integer>mapPack = new HashMap<>();
	
	
	public Agenzia(String nomeServizio, String nomeAgenzia, int molt, List<Pack> list) {
		super(nomeServizio);
		this.setListaPack(list);
		this.setNomeAgenzia(nomeAgenzia);
		this.setMoltAgenzia(molt);
		
		for(Pack pack: list) {
			cntmap++;
			mapPack.put(pack,cntmap);
					
		}
	}

	public List<Pack> getListaPack() {
		return listaPack;
	}

	public void setListaPack(List<Pack> listaPack) {
		this.listaPack = listaPack;
	}

	public String getNomeAgenzia() {
		return nomeAgenzia;
	}

	public void setNomeAgenzia(String nomeAgenzia) {
		this.nomeAgenzia = nomeAgenzia;
	}
	
	public void addPack(Pack p) {
		listaPack.add(p);
		mapPack.put(listaPack.get(cntmap),cntmap);
		cntmap++;
		
	}
	
	public void removePack(Pack p) {
		listaPack.remove(p);
		mapPack.clear(); // RESET MAPPA
		cntmap=0;
		
		for(Pack pack: this.getListaPack()) {
			mapPack.put(pack,cntmap); // CREAZIONE NUOVA MAPPA AGGIORNATA
			cntmap++;		
		}
		
	}
	
	public Map<Pack,Integer> getMapPack(){
		return this.mapPack;
	}

	public int getMoltAgenzia() {
		return moltAgenzia;
	}

	public void setMoltAgenzia(int moltAgenzia) {
		this.moltAgenzia = moltAgenzia;
	}



}
