import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Play {

	public static void execute(List<Party> listaParty, List<Servizio> servizi, Scanner s) throws InterruptedException {
		Random random = new Random();

		System.out.println("\n\n///////////////////////////////////");
		System.out.println("****** SIMULAZIONE INIZIATA *******");
		System.out.println("///////////////////////////////////\n\n");

		for (Day.getDay(); Day.getDay() < Inputs.getMaxGiorni() + 1; Day.incrementa()) {

			System.out.println("\n/////////////GIORNO " + Day.getDay() + " /////////////\n");
			for (Party party : listaParty) {

				System.out.println("\n*** TOCCA A " + party.getNome() + " ***\n");
				System.out.println("(Budget disponibile: " + party.getBudgetParz() + ")");
				Play.chooseAction(party, servizi, s);
				System.out.println("\n*** TURNO TERMINATO***\n");
			}

		}

		// FASE DELLE VOTAZIONI VERE E PROPRIE
		for (Party party : listaParty) {
			int maxBU = (int) (party.getBU() + (party.getBU() / (0.1)));
			int minBU = (int) (party.getBU() - (party.getBU() / (0.3)));

			party.setVotiTot(random.nextInt((maxBU - minBU) + 1));
		}

		System.out.println("\n\n///////////////////////////////////");
		System.out.println("****** SIMULAZIONE CONCLUSA! *******");
		System.out.println("///////////////////////////////////");
		System.out.println("******      RISULTATI:      *******");
		System.out.println("///////////////////////////////////\n\n");

		for (Party party : listaParty) {
			System.out.println(party.getNome() + "--->" + party.getVotiTot());
		}

		System.out.println("*** METODO D'HONDT ***");
		Dhondt.execute(listaParty, Inputs.getNumSeggi());
		
		Day.reset();

	}

	private static void chooseAction(Party party, List<Servizio> servizi, Scanner s) throws InterruptedException {

		Random random = new Random();
		int cnt = 0;

		while (true) {

			Pack pack;
			Agenzia agenzia;
			
			
			if (cnt == 3) {

				System.out.println("*** TEMPO SCADUTO! FINISCE LA GIORNATA PER LA LISTA " + party.getNome() + " ***");
				break;
			}

			byte ch; // SCELTA INDEX SERVIZIO
			
			if (party.isPlayable()) {
				
				ch = Inputs.chooseService(servizi, s);
				
			} else	ch = (byte) (random.nextInt(3));
			
			
			
			
			if (ch != 3) 
			{ // SE VIENE SELEZIONATO UN SERVIZIO (E NON SI DECIDE DI USCIRE/ANNULLARE)


				if (party.isPlayable()) {

					agenzia = Inputs.chooseAgenzia(servizi, ch, s);  // SCELTA MANUALE AGENZIA DATO L'INDICE DEL SERVIZIO

				} else {
					byte agch = (byte) random.nextInt(servizi.get(ch).getListaAgenzie().size()); // SCELTA AUTOMATICA AGENZIA
					agenzia = servizi.get(ch).getListaAgenzie().get(agch);
				}

				
				

				if (party.isPlayable()) { 
					
					pack = Inputs.choosePack(agenzia, s); // SCELTA MANUALE PACK DATA L'AGENZIA
					
				} else {
					
					byte agpk = (byte) random.nextInt(agenzia.getListaPack().size());  // SCELTA AUTOMATICA PACK
					pack = agenzia.getListaPack().get(agpk);
				}
				
				
				
				// *** FASE DI ACQUISTO ***
				
				

				int prezzo = (int) (pack.getPrezzo() * Day.getDailyMolt() * agenzia.getMoltAgenzia());
				int minBU = (int) (pack.getMinBU() * Day.getDailyMolt() * agenzia.getMoltAgenzia());
				int maxBU = (int) (pack.getMaxBU() * Day.getDailyMolt() * agenzia.getMoltAgenzia());
				int BU = random.nextInt((maxBU - minBU) + 1);

				if (party.getBudgetParz() < prezzo) {
					
					System.out.println("*** PREZZO TROPPO ALTO! ***");
					
					cnt++; // CONTATORE ITERAZIONI ECCESSIVE
				} else {

					System.out.println("*** PACCHETTO " + pack.getNome() + " DELL'AGENZIA " + agenzia.getNomeAgenzia()
							+ ", SERVIZIO DI " + servizi.get(ch).getNomeServizio() + ", ACQUISTATO ! ***");           
					party.setBudgetParz(party.getBudgetParz() - prezzo); 			// COMMIT ACQUISTO
					party.setBU(BU);
					Thread.sleep(1000);
					break;
				}

			} else {
				System.out.println("*** " + party.getNome() + " OGGI NON FA NIENTE! ***");
				Thread.sleep(1000);
				break;
			}
		}

	}

}
