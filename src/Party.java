import java.util.List;
import java.util.ArrayList;

public class Party implements Caratteristiche {
	
	private int votiTot;
	private int votiParz;
	private int seggi = 0;
	private int budget=0;
	private int budgetParz;
	private boolean playable;
	private String nome = "N.P";
	private String orientamento = "N.P.";
	private String leader = "N.P.";
	private int BU = 0;
	
	public Party() {
		this.votiTot=0;
	}
	
	public Party(int voti) {
		this.votiTot=voti;
	}
	
	public Party(String nome) {
		this.nome=nome;
	}
	
	public Party(String nome, int voti) {
		this.votiTot=voti;
		this.nome=nome;
	}
	
	public Party(String nome, int voti, int budget,boolean playable) {
		
		this.votiTot=voti;
		this.nome=nome;
		this.budget=budget;

		this.budgetParz=budget;
		this.playable=playable;
	}
	
	public Party(String nome, int voti, int budget,boolean playable, String leader, String orientamento) {
		
		this.leader=leader;
		this.orientamento=orientamento;
		this.votiTot=voti;
		this.nome=nome;
		this.budget=budget;
		this.budgetParz=budget;
		this.playable=playable;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setBudget(int budget) {
		this.budget=budget;
	}
	
	public void setVotiTot(int voti) {
		this.votiTot=voti;
	}
	
	public void setVotiParz(int voti) {
		this.votiParz=voti;
	}
	
	public void setSeggi(int seggi) {
		this.seggi=seggi;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public int getBudget() {
		return this.budget;
	}
	
	public int getVotiTot() {
		return this.votiTot;
	}
	
	public int getVotiParz() {
		return this.votiParz;
	}

	public int getSeggi() {
		return this.seggi;
	}
	

	public void initVotiParz() {
		
		this.votiParz = this.votiTot;
	} 

	public int getBudgetParz() {
		return budgetParz;
	}

	public void setBudgetParz(int budgetParz) {
		this.budgetParz = budgetParz;
	}
	
	public List<Party> getC(){
		
		return new ArrayList<Party>();
		
	}

	public boolean isPlayable() {
		return playable;
	}

	public void setPlayable(boolean playable) {
		this.playable = playable;
	}

	public String getOrientamento() {
		return orientamento;
	}

	public void setOrientamento(String orientamento) {
		this.orientamento = orientamento;
	}

	public String getLeader() {
		return leader;
	}

	public void setLeader(String leader) {
		this.leader = leader;
	}

	@Override
	public void stampaCaratteristiche() {

		System.out.println("*****************************");
		System.out.println("*******CARATTERISTICHE*******");
		System.out.println("*****************************");
		System.out.println(this.getNome());
		System.out.println("*****************************");
		System.out.println("Leader: " + this.getLeader());
		System.out.println("Orientamento Politico: " + this.getOrientamento());
		
		
	}

	public int getBU() {
		return BU;
	}

	public void setBU(int bU) {
		BU = bU;
	}
	
}
