import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Servizio {
	
	private Integer cntmap=0;
	
	private String nomeServizio;
	
	private List<Agenzia>listaAgenzie = new ArrayList<>();
	private Map<Agenzia,Integer>mapAgenzie = new HashMap<>();
	
	public Servizio(String nome) {
		
		this.nomeServizio = nome;
		
		
	}
	

		

	public List<Agenzia> getListaAgenzie(){
		return listaAgenzie;
	}
	
	public void setListaAgenzie(List<Agenzia> list) {
		this.listaAgenzie = list;
	}
	

	public String getNomeServizio() {
		return nomeServizio;
	}

	public void setNomeServizio(String nome) {
		this.nomeServizio = nome;
	}
	
	public void addAgenzia(String nome, int molt, List<Pack> list) {
		listaAgenzie.add(new Agenzia(this.nomeServizio,nome,molt,list));
		mapAgenzie.put(listaAgenzie.get(cntmap),cntmap);
		cntmap++;
	}
	
	public void removeAgenzia(Agenzia p) {
		listaAgenzie.remove(p);
		mapAgenzie.clear(); // RESET MAPPA
		cntmap=0;
		
		for(Agenzia agenzia: this.getListaAgenzie()) {
			mapAgenzie.put(agenzia,cntmap); // CREAZIONE NUOVA MAPPA AGGIORNATA
			cntmap++;		
		}
		
	}
	
	public Map<Agenzia,Integer> getMapAgenzie(){
		return this.mapAgenzie;
	}
}
