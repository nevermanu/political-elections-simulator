import java.util.Comparator;

public class SortListePerSeggi implements Comparator<Party> {

	@Override
	public int compare(Party p1, Party p2) {
		
		if(p1.getSeggi()<p2.getSeggi()) return 1;
		
		else if(p1.getSeggi()==p2.getSeggi()) return 0;
		
		else return -1;
		
	}
	


}