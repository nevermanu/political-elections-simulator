import java.util.List;
import java.util.Scanner;

public class Options {
	
	public static void launch(List<Servizio> servizi, Scanner s) {
		
		while(true) {
			byte ch;
			System.out.println("\n\n///////////////////////////////////");
			System.out.println("********** IMPOSTAZIONI ***********");
			System.out.println("///////////////////////////////////");
			System.out.println("*** COSA VUOI FARE? ***\n\n");
			System.out.println("\n\n/////////////////////////////////");
			System.out.println("(0) TORNA AL MENU PRINCIPALE");
			System.out.println("(1) MODIFICA/AGGIUNGI/RIMUOVI AGENZIA ");
			System.out.println("(2) MODIFICA/AGGIUNGI/RIMUOVI PACCHETTO");
			System.out.println("(3) MODIFICA NUMERO SEGGI SIMULAZIONE");
			System.out.println("(4) MODIFICA BUDGET INIZIALE SIMULAZIONE ");
			System.out.println("(5) MODIFICA BUDGET MASSIMO SIMULAZIONE");
			System.out.println("(6) MODIFICA NUMERO DI GIORNI SIMULAZIONE ");
			System.out.println("\n\n/////////////////////////////////");
			ch = s.nextByte();
			
			switch(ch) {
				case 0: return;
				
				case 1:
					Options.chooseModAgenzie(servizi, s);
					break;
					
				case 2:
					
					Options.chooseModPack(servizi, s);
					break;
					
				case 3:
					System.out.println("*** INSERISCI NUOVO NUMERO DI SEGGI (ATTUALE: " + Inputs.getNumSeggi() + ")");
					Inputs.setNumSeggi(s.nextInt());
					break;
					
				case 4:
					System.out.println("*** INSERISCI NUOVO BUDGET DI PARTENZA (ATTUALE: " + Inputs.getStartBudget() + ")");
					Inputs.setStartBudget(s.nextInt());
					break;
					
				case 5:
					System.out.println("*** INSERISCI NUOVO BUDGET MASSIMO (ATTUALE: " + Inputs.getMaxBudget() + ")");
					Inputs.setMaxBudget(s.nextInt());
					break;
					
				case 6:
					System.out.println("*** INSERISCI GIORNI DI SIMULAZIONE (ATTUALI: " + Inputs.getMaxGiorni() + ")");
					Inputs.setMaxGiorni(s.nextInt());
					break;
				
				default: return;
			}
		
		}
		
	}
	

	
	private static void chooseModPack(List<Servizio> servizi, Scanner s) {
		
		System.out.println("*** COSA VUOI FARE? ***\n\n");
		System.out.println("\n\n/////////////////////////////////");
		System.out.println("(0) ANNULLA");
		System.out.println("(1) MODIFICA PACCHETTO ");
		System.out.println("(2) AGGIUNGI PACCHETTO");
		System.out.println("(3) RIMUOVI PACCHETTO ");
		System.out.println("/////////////////////////////////\n\n");
		
		byte ch = s.nextByte();
		
		switch(ch) {
			case 0: break;
			
			case 1:
				ch = Inputs.chooseService(servizi, s);
				Agenzia agenzia = Inputs.chooseAgenzia(servizi, ch, s);
				Pack pack = Inputs.choosePack(agenzia, s);
				
				System.out.println("*** COSA VUOI FARE? ***\n\n");
				System.out.println("\n\n/////////////////////////////////");
				System.out.println("(0) ANNULLA");
				System.out.println("(1) MODIFICA NOME (NOME ATTUALE: " + pack.getNome() + ")");
				System.out.println("(2) MODIFICA BACINO DI UTENZA MINIMO (minBU ATTUALE: " + pack.getMinBU() + ")");
				System.out.println("(3) MODIFICA BACINO DI UTENZA MASSIMO (maxBU ATTUALE: " + pack.getMaxBU() + ")");
				System.out.println("(4) MODIFICA PREZZO (PREZZO ATTUALE: " + pack.getPrezzo() + ")");	
				System.out.println("/////////////////////////////////\n\n");
				
				byte b = s.nextByte();
				
				switch(b) {
					case 0: break;
					
					case 1:
						s.nextLine();
						System.out.println("*** INSERISCI NUOVO NOME PACCHETTO: ***");
						pack.setNome(s.nextLine());
						break;
					case 2:
						System.out.println("*** INSERISCI NUOVO BACINO DI UTENZA MINIMO: ***");			// MODIFICA PACCHETTO
						pack.setMinBU(s.nextInt());
						break;
					case 3:
						System.out.println("*** INSERISCI NUOVO BACINO DI UTENZA MASSIMO: ***");
						pack.setMaxBU(s.nextInt());
						break;
					case 4:
						System.out.println("*** INSERISCI NUOVO PREZZO PACCHETTO: ***");
						pack.setPrezzo(s.nextInt());
						break;
						
					default: break;
				}
				
			case 2:
				byte i = Inputs.chooseService(servizi, s);
				
				if(i==3) break; // SE SI E' SCELTO IL N.3 NELLA SCELTA DEL SERVIZIO (ERGO ANNULLA)
				
				Agenzia age = Inputs.chooseAgenzia(servizi, i, s);
				
				s.nextLine();
				System.out.println("*** INSERISCI NOME NUOVO PACCHETTO: ***");
				String nome = s.nextLine();
				System.out.println("*** INSERISCI BACINO DI UTENZA MINIMO NUOVO PACCHETTO: ***");		// INSERIMENTO NUOVO PACCHETTO
				int minBU = s.nextInt();
				System.out.println("*** INSERISCI BACINO DI UTENZA MASSIMO NUOVO PACCHETTO: ***");
				int maxBU = s.nextInt();
				System.out.println("*** INSERISCI PREZZO NUOVO PACCHETTO: ***");
				int prezzo = s.nextInt();
				
				age.addPack(new Pack(nome, minBU, maxBU, prezzo));
				
				break;
				
			case 3:
				byte v = Inputs.chooseService(servizi, s);
				if (v==3) break; // SE SI E' SCELTO IL N.3 NELLA SCELTA DEL SERVIZIO (ERGO ANNULLA)
				Agenzia ag = Inputs.chooseAgenzia(servizi, v, s);
				Pack pk = Inputs.choosePack(ag, s);
				ag.removePack(pk);
	
			default: break;
		}
		return;
	}
		



	private static void chooseModAgenzie(List<Servizio> servizi, Scanner s) {
		
		System.out.println("*** COSA VUOI FARE? ***\n\n");
		System.out.println("\n\n/////////////////////////////////");
		System.out.println("(0) ANNULLA");
		System.out.println("(1) MODIFICA AGENZIA ");
		System.out.println("(2) AGGIUNGI AGENZIA");
		System.out.println("(3) RIMUOVI AGENZIA ");
		System.out.println("/////////////////////////////////\n\n");
		
		byte ch = s.nextByte();
		
		switch(ch) {
			case 0: break;
			
			case 1:
				byte d;
				d = Inputs.chooseService(servizi, s);
				if(d==3) break; // SE SI E' SCELTO IL N.3 NELLA SCELTA DEL SERVIZIO (ERGO ANNULLA)
					
				Agenzia agenzia = Inputs.chooseAgenzia(servizi, d, s);
				System.out.println("*** COSA VUOI FARE? ***\n\n");
				System.out.println("\n\n/////////////////////////////////");
				System.out.println("(0) ANNULLA");
				System.out.println("(1) MODIFICA NOME (NOME ATTUALE: " + agenzia.getNomeAgenzia() + ")");
				System.out.println("(2) MODIFICA MOLTIPLICATORE (MOLTIPLICATORE ATTUALE: x" + agenzia.getMoltAgenzia() + ")");
				System.out.println("/////////////////////////////////\n\n");
				
				byte b = s.nextByte();
				switch(b) {
					case 0: break;
					
					case 1:
						s.nextLine();
						System.out.println("*** INSERISCI NUOVO NOME: ***");
						agenzia.setNomeAgenzia(s.nextLine());
						break;
					case 2:
						System.out.println("*** INSERISCI NUOVO MOLTIPLICATORE: ***");
						agenzia.setMoltAgenzia(s.nextInt());
						break;
					default: break;
				}
				break;
				
			case 2:
				byte i = Inputs.chooseService(servizi, s);
				
				if(i==3) break; // SE SI E' SCELTO IL N.3 NELLA SCELTA DEL SERVIZIO (ERGO ANNULLA)
				
					s.nextLine();
					System.out.println("*** INSERISCI NOME AGENZIA: ***");
					String nome = s.nextLine();
					System.out.println("*** INSERISCI MOLTIPLICATORE:   ***");
					int molt = s.nextInt();
					List<Pack> list = Inputs.createSetPack(s);
					servizi.get(i).addAgenzia(nome, molt, list);
				
					break;
				
			case 3:
				byte v = Inputs.chooseService(servizi, s);
				if(v==3) break; // SE SI E' SCELTO IL N.3 NELLA SCELTA DEL SERVIZIO (ERGO ANNULLA)
				
				Agenzia ag = Inputs.chooseAgenzia(servizi, v, s);
				servizi.get(v).removeAgenzia(ag);
				break;
	
			default: break;
		}
		
		return;
	}
}
